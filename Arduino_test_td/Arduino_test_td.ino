

/*---------Exo3-----*/
/*int pinPot=0; //variable pour définir le CAN où est connecté le potentiomètre
int valPot=0; //variable pour récupérer la tension aux bornes du potentiomètre traduite par le CAN . On l’initialise à 0.
void setup() {
  Serial.begin(9600); //Initialisation de la communication avec la console
}

void loop() {
  valPot=analogRead(A0); //lit la tension, la convertit en valeur numérique et la stocke dans valeurPot
  Serial.print("Valeur lue : ");
  Serial.println(valPot);
} */


/*---------Exo4--------*/

/*int pinPot=0; //variable pour le CAN utilisé
int valPot=0; //variable qui va stocker la tension lue. On l'initialise à 0.
int pinLED=8; // pin de connexion de la LED
void setup() {
  pinMode(pinLED,OUTPUT); //Mode OUTPUT pour le pin de LED
  digitalWrite(pinLED,HIGH);//On allume la LED
}

void loop() {
  valPot=analogRead(A0); //lit la valeur de tension, la transforme et la stocke dans la variable
  int attente=valPot; //le délai d'attente est la valeur du potentiomètre
  digitalWrite(pinLED,HIGH); //on allume la LED
  delay(attente); //on attend en fonction de la variable attente
  digitalWrite(pinLED,LOW); //on éteint la LED
  delay(attente); //on attend
}*/

/*--------Touch V1.1 touch allume led---------*/
#include <Wire.h>
#include "rgb_lcd.h"
#include <math.h>

#if defined(ARDUINO_ARCH_AVR)
#define debug  Serial
#elif defined(ARDUINO_ARCH_SAMD) ||  defined(ARDUINO_ARCH_SAM)
#define debug  SerialUSB
#else
#define debug  Serial
#endif
//lcd
rgb_lcd lcd; 
//température

const int B = 4275;               // B value of the thermistor
const int R0 = 100000;            // R0 = 100k
const int pinTempSensor = A0;     // Grove - Temperature Sensor connect to A0
//lcd

const int colorR = 255;
const int colorG = 0;
const int colorB = 0;
const int TouchPin=2;
const int ledPin=3;

void setup() {
  Serial.begin(9600); //Initialisation de la communication avec la console
  lcd.begin(16, 2);
  lcd.setRGB(colorR, colorG, colorB);
  pinMode(TouchPin, INPUT);
  pinMode(ledPin,OUTPUT);
}

void loop() {
  lcd.setCursor(0, 1);
    int sensorValue = digitalRead(TouchPin);
    if(sensorValue==1)
    {
        digitalWrite(ledPin,HIGH);
        lcd.print("La cle n a ete prise");
        delay(100);
    }
    else
    {
        digitalWrite(ledPin,LOW);
        lcd.clear();
        delay(100);
    }
    {
    int a = analogRead(pinTempSensor);
    float R = 1023.0/a-1.0;
    R = R0*R;
    float temperature = 1.0/(log(R/R0)/B+1/298.15)-273.15; // convert to temperature via datasheet
    Serial.print("temperature = ");
    Serial.println(temperature);
    delay(5000); //soucis délai s'applique a tout meme on souhaite qu'il ne s'applique a a l'actualisation de la température
    }
    }
}
/*--------------*/
